package Keyboard;

//import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"Keyboard"})
public class AppConfig {

//    @Bean(name = "keyboard")
//    public Keyboard getKeyboard() {
////        return new Keyboard(getKeyWithLetter());
//        Keyboard keyboard = new Keyboard();
////        keyboard.setKeyWithLetter(getKeyWithLetter());
//        return keyboard;
//    }

//    @Bean(name = "keyWithLetter")
//    public KeyWithLetter getKeyWithLetter() {
//        return KeyWithLetter.getInstance();
//    }
}
