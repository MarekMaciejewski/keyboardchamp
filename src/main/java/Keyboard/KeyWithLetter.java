package Keyboard;

import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Repository;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 <b>KeyWithLetter</b> - picture reading, letter generation, drawing and scaling
 @author Marek Maciejewski
 @version 2.0
 */

//@Repository("keyWithLetter")
@Component("keyWithLetter")
class KeyWithLetter {
    private static final int SIZE = 200;

    private JFrame key = new JFrame();
    private Image img = Toolkit.getDefaultToolkit().getImage("src\\main\\resources\\Button.png");
    private Graphics2D graphics;

    private KeyWithLetter() {
        try {
            MediaTracker mt = new MediaTracker(key);
            mt.addImage(img, 0);
            mt.waitForID(0);
        } catch (Exception e) {
            System.out.println("Error while adding the Image to MediaTracker: " + e);
        }
    }

    private static KeyWithLetter instance = new KeyWithLetter();

    static KeyWithLetter getInstance() {return instance;}

    private void drawLetterOnImage(String s) {
        graphics.drawImage(img,0,0, key);
        graphics.setColor(Color.black);
        graphics.setFont(new Font("Times New Roman",Font.BOLD, SIZE));
        graphics.drawString (s,75,175);
    }

    Image getImgWithTheLetter(final int letterASCIICode) {
        BufferedImage bufferedImage = new BufferedImage(img.getWidth(key), img.getHeight(key), BufferedImage.TYPE_INT_ARGB);
        graphics = bufferedImage.createGraphics();
        drawLetterOnImage(Character.toString((char) letterASCIICode));
        return bufferedImage.getScaledInstance(Constants.BUTTON_WIDTH, Constants.BUTTON_HEIGHT, 0);
    }
}
