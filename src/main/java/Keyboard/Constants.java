package Keyboard;

import java.awt.*;

class Constants {
    private Constants(){}

    static final Dimension DIMENSION = Toolkit.getDefaultToolkit().getScreenSize();
    static final int FRAME_WIDTH = DIMENSION.width * 3/4;
    static final int FRAME_HEIGHT = DIMENSION.height * 3/4;
    static final int FRAME_HORIZONTAL_LOCATION = DIMENSION.width/8;
    static final int FRAME_VERTICAL_LOCATION = DIMENSION.height/8;
    static final int BUTTON_WIDTH = 50;
    static final int BUTTON_HEIGHT = 50;
    static final int VERTICAL_POSITION_DELTA = (FRAME_HEIGHT - BUTTON_HEIGHT)/20;
    static final int DELAY_DELTA = 50;
    static final int MAX_ACCEPTABLE_SLIPS = 5;
    static final int NEXT_LEVEL_TRIGGER = 5;

    static final String TITLE = "Keyboard Champ";
    static final String INTRO_MESSAGE = "Get ready!";
    static final String INTRO_MESSAGE_TITLE = "Information";
    static final String GAME_OVER_MESSAGE = "You lose!";
    static final String GAME_OVER_MESSAGE_TITLE = "Game Over";
    static final String LABEL_1_TEXT = "Level: ";
    static final String LABEL_2_TEXT = "Slips: ";
}
