package Keyboard;

interface RandomObjectGenerator<T> {
    T generate();
}
