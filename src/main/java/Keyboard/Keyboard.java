package Keyboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;
import static Keyboard.Constants.*;

/**
<b>Keyboard</b> - main window, event handling and animation thread
@author Marek Maciejewski
@version 2.0
*/
//@Service("keyboard")
@Component("keyboard")
public class Keyboard implements Runnable{

    private int randomASCIICode;
    private int horizontalPosition = FRAME_WIDTH/2 - BUTTON_WIDTH/2;
    private int verticalPosition = 0;
    private int delay = 1000;
    private int level = 0;

    private volatile boolean isGameOver = false;

    private JFrame frame;
	private JButton button;
	private JLabel label1, label2;

    private KeyWithLetter keyWithLetter;
    private Thread animation;
    private Random gen;

    private static ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);

    Keyboard() {
        System.out.println("Constructor with no parameters");
        frame = new JFrame(TITLE);
        label1 = new JLabel(LABEL_1_TEXT + level);
        label2 = new JLabel(LABEL_2_TEXT + MAX_ACCEPTABLE_SLIPS);
        gen = new Random();
        generateNewRandomASCIICode();
        animation = new Thread(this);

    }

    Keyboard(KeyWithLetter keyWithLetter) {
        System.out.println("Constructor with a parameter");
        this.keyWithLetter = keyWithLetter;
        frame = new JFrame(TITLE);
        label1 = new JLabel(LABEL_1_TEXT + level);
		label2 = new JLabel(LABEL_2_TEXT + MAX_ACCEPTABLE_SLIPS);
        gen = new Random();
		generateNewRandomASCIICode();
        animation = new Thread(this);
	}

    public static void main(String arg[]) throws Exception{
        Keyboard keyboard = appContext.getBean("keyboard", Keyboard.class);
        keyboard.setFrame();
        keyboard.setLabels();
        keyboard.addLabelsToFrame();
        keyboard.setButton();
        keyboard.keyListenerLogic();
        keyboard.addButton();
        keyboard.showMessage(INTRO_MESSAGE, INTRO_MESSAGE_TITLE);
        keyboard.startAnimation();
    }

    @Autowired
    void setKeyWithLetter(KeyWithLetter keyWithLetter) {
        System.out.println("Setter");
        this.keyWithLetter = keyWithLetter;
    }

    private void generateNewRandomASCIICode() {
        RandomObjectGenerator<Integer> rog = () -> {
            int randomLetterASCIICode = gen.nextInt(36)+48;
            if (randomLetterASCIICode > 57) randomLetterASCIICode += 7;
            return randomLetterASCIICode;
        };
        randomASCIICode = rog.generate();
    }

    private void generateNewHorizontalPosition() {
        RandomObjectGenerator<Integer> rog = () -> gen.nextInt(Constants.FRAME_WIDTH - Constants.BUTTON_WIDTH);
        horizontalPosition = rog.generate();
    }

    private void setFrame() {
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLocation(FRAME_HORIZONTAL_LOCATION, FRAME_VERTICAL_LOCATION);
        frame.setLayout(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void setLabels() {
        label1.setBounds(10,10, DIMENSION.width/10, DIMENSION.height/50);
        label2.setBounds(10,10 + DIMENSION.height/50, DIMENSION.width/10, DIMENSION.height/50);
        label1.setFont(new Font("Times New Roman", Font.BOLD, DIMENSION.height/50));
        label2.setFont(new Font("Times New Roman", Font.BOLD, DIMENSION.height/50));
        label1.setOpaque(false);
        label2.setOpaque(false);
    }

    private void addLabelsToFrame() {
        frame.add(label1);
        frame.add(label2);
    }

    private void setButton() {
        button = new JButton(new ImageIcon(keyWithLetter.getImgWithTheLetter(randomASCIICode)));
        button.setBackground(Color.white);
        button.setBounds (horizontalPosition, verticalPosition, BUTTON_WIDTH, BUTTON_HEIGHT);
        button.setOpaque(true);
    }

    private void keyListenerLogic() {
        button.addKeyListener(new KeyAdapter() {
            private int nextLevelCounter = 0;
            private int slipsCounter = MAX_ACCEPTABLE_SLIPS;

            public void keyPressed(KeyEvent ke) {
                if (isKeyCodeEqualToRandomCode(ke) && animation.isAlive()) {
                    generateNewRandomASCIICode();
                    generateNewHorizontalPosition();
                    verticalPosition = 0;
                    button.setIcon(new ImageIcon(keyWithLetter.getImgWithTheLetter(randomASCIICode)));
                    button.setBounds (horizontalPosition, verticalPosition, BUTTON_WIDTH, BUTTON_HEIGHT);
                    nextLevelCounter++;
                    if ((nextLevelCounter >= NEXT_LEVEL_TRIGGER) && (delay > DELAY_DELTA)) {
                        increaseGameSpeed();
                        level++;
                        label1.setText(LABEL_1_TEXT + level);
                        nextLevelCounter = 0;
                    }
                    return;
                }
                slipsCounter--;
                if (delay > DELAY_DELTA) increaseGameSpeed();
                if (slipsCounter >= 0) {
                    label2.setText(LABEL_2_TEXT + slipsCounter);
                    return;
                }
                isGameOver = true;
            }

            private void increaseGameSpeed() {
                delay -= DELAY_DELTA;
            }

            private boolean isKeyCodeEqualToRandomCode(KeyEvent ke) {
                return ke.getKeyCode() == randomASCIICode;
            }
        });
    }

    private void addButton() {
        frame.add(button);
    }

    private void showMessage(final String message, final String messageTitle) {
        JOptionPane.showMessageDialog(frame, message, messageTitle, JOptionPane.INFORMATION_MESSAGE);
    }

    private void startAnimation() {
        animation.start();
    }

    public void run() {
		while (!isGameOver) {
			decreaseVerticalPosition();
			button.setBounds (horizontalPosition, verticalPosition, BUTTON_WIDTH, BUTTON_HEIGHT);
			if (isButtonOnBottom()) break;
			try {
			    Thread.sleep(delay);
			}
			catch (Throwable t) {
			    throw new OutOfMemoryError("An Error has occurred.");
			}
		}
		showMessage(GAME_OVER_MESSAGE, GAME_OVER_MESSAGE_TITLE);
		System.exit(0);
	}

    private void decreaseVerticalPosition() {
        verticalPosition += VERTICAL_POSITION_DELTA;
    }

    private boolean isButtonOnBottom() {
        return verticalPosition >= FRAME_HEIGHT - 100;
    }
}
