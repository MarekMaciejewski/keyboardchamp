import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;

/**
 Obraz - wczytanie obrazka, naniesienie losowej litery i przeskalowanie
 @author Marek Maciejewski
 @version 1.0
 */

class Obraz extends JFrame{
    Image img;
    static Image img2;		//obrazek do przycisku z wrysowana losowa litera
    int r, w, h;

    //Wczytanie obrazka Button.png
    Obraz(){
        img = Toolkit.getDefaultToolkit().getImage("Button.png");
        try {									//p�tla w kt�rej czekamy na skonczenie produkcji obrazu dla obiektu img
            MediaTracker mt = new MediaTracker(this);
            mt.addImage(img, 0);
            mt.waitForID(0);
        } catch (Exception e) {}

        w = img.getWidth(this);						//wymiary obrazka
        h = img.getHeight(this);
    } //void Obraz()

    //Utworzenie buforu obrazu img3 i wrysowanie losowej litery do kontekstu graficznego img
    void litera(){
        Random gen = new Random();					//generator liczb losowych (nowy obiekt klasy Random)
        do{r = gen.nextInt(42)+48;}					//generacja losowego kodu litery
        while(r >= 58 && r <= 64);					//z zakres�w: 0-9 i A-Z
        String s = Character.toString((char)r);		//zamiana kodu na odpowiadajaca mu litere

        BufferedImage img3 = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);	//utorzenie bufora
        Graphics2D g = img3.createGraphics();										//pobranie kontekstu graficznego
        g.drawImage(img,0,0,this);													//wrysowanie obrazu do bufora
        g.setColor(Color.black);													//ustawienie koloru do rysowania
        g.setFont(new Font("Times New Roman",Font.BOLD,200));						//i czcionki do pisania
        g.drawString (s,75,175);													//wrysowanie litery do bufora

        img2 = img3.getScaledInstance(50, 50, 0);	//przeskalowanie do rozmiarow przycisku
    } //void litera()
} //class Obraz

/**
 <b>Klawiatura</b> - glowne okno i obsluga zdarzen oraz watek animacji
 @author Marek Maciejewski
 @version 1.0
 */

public class Klawiatura extends JFrame implements Runnable{

    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();		//wymiary ekranu
    JButton b; JLabel lab1, lab2;									//komponenty w JFrame
    int i, j=0;														//pozycje JButton
    int count1=0, count2=5, poziom=0;								//Liczniki m.in. dla JLabel
    int level=1000;													//czas opoznienia dla watku w ms
    boolean whi = true;												//zmienna logiczna do petli watku
    Obraz o;
    Random var;
    Thread animacja;

    //Zainicjowanie glowniego okna wraz z komponentami i obsluga zdarzen oraz wystartowanie watku animacji
    public Klawiatura (String tytul){
        super(tytul);
        setSize(d.width*3/4, d.height*3/4);							//wymiary = 3/4 ekranu
        setLocation(d.width*1/8, d.height*1/8);						//wysrodkowanie okna
        setLayout(null);
        lab1 = new JLabel("Level: 0");								//poczatkowe napisy na etykietach
        lab2 = new JLabel("Slips: 4");
        lab1.setBounds(10,10,d.width/10,d.height/50);				//ustawienie lokacji i wymiarow etykiet
        lab2.setBounds(10,10+d.height/50,d.width/10,d.height/50);
        lab1.setFont(new Font("Times New Roman",Font.BOLD,d.height/50)); 	//ustawienie czcionek dla etykiet
        lab2.setFont(new Font("Times New Roman",Font.BOLD,d.height/50));
        lab1.setOpaque(false);										//ustawienie przezroczystosci na etykietach
        lab2.setOpaque(false);
        add(lab1);													//dodanie etykiet
        add(lab2);
        setResizable(false);										//brak mozliwosci zmiany rozmiarow okna
        setDefaultCloseOperation(EXIT_ON_CLOSE);					//domyslna akcja przy zamknieciu okna

        i = d.width*3/8-25;		//poczatkowa lokacja przycisku w srodku okna
        o = new Obraz();		//stworzenie nowego obiektu klasy Obraz (wczytanie obrazka)
        o.litera();				//wywolanie metody litera() (wygenerowanie losowej litery na przycisku)
        var = new Random();

        b = new JButton(new ImageIcon(Obraz.img2));					//przycisk z wygenerowanym obrazkiem
        b.setBackground(Color.white);								//ustawienie tla przycisku
        b.setBounds (i, j, 50, 50);									//ustawienie poczatkowej lokacji (gora, srodek)
        b.setOpaque(true);											//ustawienie nieprzezroczystosci dla JButton
        b.addKeyListener(new KeyAdapter(){							//dodanie obslugi zdarzen na klawiaturze
            public void keyPressed(KeyEvent hm){
                if((hm.getKeyCode()==o.r)&&(animacja.isAlive())){	//jezeli kod przycisnietego klawisza jest zgodny z losowo wygenerowanym dla obrazka, a watek animacji wciaz jest aktywny,
                    o.litera();										//to wygeneruj nowa litere dla obrazka img2,
                    i = var.nextInt(d.width*3/4-50);				//wygeneruj nowa losowa lokacje w poziomie,
                    j = 0;											//ustaw lokacje w pionie na gore okna
                    b.setIcon(new ImageIcon(Obraz.img2));			//i ustaw nowy obrazek
                    b.setBounds (i, j, 50, 50);						//oraz przenies przycisk w nowa lokacje
                    count1++;										//inkrementacja licznika
                    if ((count1>=5)&&(level>50)){					//je�eli powyzszy licznik osiagnie wartosc 5 a czas opoznienia jest wiekszy od 50,
                        level-=50;									//to przyspiesz animacje,
                        poziom++;									//zwieksz poziom
                        lab1.setText(new String("Level: "+poziom));	//i wyswietl go na etykiecie lab1
                        count1=0;									//oraz wyzeruj powyzszy licznik
                    } //if
                } //if
                else{												//jezeli zostanie nacisniety bledny klawisz
                    count2--;										//zmniejsz akceptowalna ilosc pomylek
                    if(level>50) level-=50;							//jezeli czas opoznienia jest wiekszy od 50, to przyspiesz animacje
                    if(count2>0) lab2.setText(new String("Slips: "+(count2-1)));	//jezeli akceptowalna ilosc pomylek jest wieksza od 0, to wyswietl ich pozostala ilosc na etykiecie lab2
                    else whi = false;								//w przeciwnym razie zatrzymaj petle watku
                } //else
            } //public void keyPressed()
        }); //KeyAdapter()
        add(b);
        setVisible(true);

        JOptionPane.showMessageDialog(this,"Get raedy!","Information",JOptionPane.INFORMATION_MESSAGE);	//wiadomosc poczatkowa
        animacja = new Thread(this);								//nowy obiekt klasy Tread
        animacja.start();											//start watku animacji
    }//Klawiatura()

    //Watek animacji (nadpisanie metody run())
    public void run(){
        while(whi){													//glowana petla watku zalezna od wartosci logicznej whi
            j+=(d.height*3/4-50)/20;								//obniz pozycje przycisku w pionie o 5% wysokosci okna
            b.setBounds (i, j, 50, 50);								//ustaw przycisk w nowej pozycji
            if(j>=d.height*3/4-100) break;							//jezeli przycisk dojdzie do konca okna, przerwij petle
            try{Thread.sleep(level);}								//uspij watek na czas podany przez zmienna level (ms)
            catch (Throwable t){throw new OutOfMemoryError("An Error has occured");}	//w razie gdyby cos
        } //while
        JOptionPane.showMessageDialog(this,"You lose!","Information",JOptionPane.INFORMATION_MESSAGE);	//monit o przegranej
        System.exit(0);												//wyjscie z programu
    } //public void run()

    public static void main(String arg[]) throws Exception{
        Klawiatura k = new Klawiatura("Keyboard Champ"); 			//stworzenie obiektu glownej klasy
    }//koniec main()
}//koniec class Witajcie